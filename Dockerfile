FROM node:6.11.3

MAINTAINER london-digital@hogarth-ogilvy.com

RUN apt-get update
RUN apt-get install -y git mercurial zip

RUN npm install -g yarn
